module.exports = function mapObject(obj, cb) {
  if (typeof cb !== "function") {
    throw new TypeError("Callback must be a function");
  }

  const result = {};

  for (const key in obj) {
    // hasOwnProperty method returns boolean value indicating whether
    // object has the specified property as its own property
    if (obj.hasOwnProperty(key)) {
      result[key] = cb(obj[key], key, obj);
    }
  }
  return result;
};

module.exports = function values(obj) {
  if (typeof obj !== "object" || obj === null) {
    throw new TypeError("Object.values called on non-object");
  }

  const values = [];

  for (const key in obj) {
    // hasOwnProperty method returns boolean value indicating whether
    // object has the specified property as its own property
    if (obj.hasOwnProperty(key) && typeof obj[key] !== "function") {
      values.push(obj[key]);
    }
  }
  return values;
};

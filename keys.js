module.exports = function keys(obj) {
  if (typeof obj !== "object" || obj === null) {
    throw new TypeError("Object.keys called on non-object");
  }

  const keys = [];
  for (const key in obj) {
    // hasOwnProperty method returns boolean value indicating whether
    // object has the specified property as its own property
    if (obj.hasOwnProperty(key)) {
      keys.push(key);
    }
  }
  return keys;
};

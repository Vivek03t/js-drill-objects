module.exports = function pairs(obj) {
  let pairsArray = [];

  for (let key in obj) {
    // hasOwnProperty method returns boolean value indicating whether
    // object has the specified property as its own property
    if (obj.hasOwnProperty(key)) {
      pairsArray.push([key, obj[key]]);
    }
  }
  return pairsArray;
};

module.exports = function pairs(obj) {
  let invertedObject = {};

  for (let key in obj) {
    // hasOwnProperty method returns boolean value indicating whether
    // object has the specified property as its own property
    if (obj.hasOwnProperty(key)) {
      invertedObject[obj[key]] = key;
    }
  }
  return invertedObject;
};

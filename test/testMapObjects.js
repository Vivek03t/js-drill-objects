const mapObject = require("../mapObject");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

const mappedPerson = mapObject(testObject, (val) => val + 5);
console.log(mappedPerson);

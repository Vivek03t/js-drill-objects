const myDefaults = require("../defaults");

const testObject = { flavor: "vanilla", sprinkles: "lots" };
const iceCream = { taste: "sweet" };

const newObject = myDefaults(testObject, iceCream);
console.log(newObject);

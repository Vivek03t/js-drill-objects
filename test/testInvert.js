const myInvert = require("../invert");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

const invertedObject = myInvert(testObject);
console.log(invertedObject);

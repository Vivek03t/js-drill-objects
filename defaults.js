module.exports = function defaults(obj, defaultProps) {
  if (typeof defaultProps !== "object" || defaultProps === null) {
    throw new TypeError("Defaults object must be a valid object");
  }

  for (let key in defaultProps) {
    // hasOwnProperty method returns boolean value indicating whether
    // object has the specified property as its own property
    if (defaultProps.hasOwnProperty(key) && obj[key] === undefined) {
      obj[key] = defaultProps[key];
    }
  }

  return obj;
};
